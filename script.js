// 1 Переменные:
// • Создайте переменную a и присвойте ей значение 3. Выведите значение этой переменной на экран.

let a = 3
console.log(a)

// • Создайте переменные a=10 и b=2. Выведите на экран их сумму, разность, произведение и частное (результат деления).

let a = 10
let b = 2
console.log(a + b)
console.log(a - b)
console.log(a * b)
console.log(a / b)

// • Создайте переменные c=15 и d=2. Просуммируйте их, а результат присвойте переменной result. Выведите на экран значение переменной result.

let c = 15
let d = 2
let result = c + d

console.log(result)

//• Создайте переменные a=10, b=2 и c=5. Выведите на экран их сумму.

let a = 10
let b = 2
let c = 5

console.log(a + b + c)

//• Создайте переменные a=17 и b=10. Отнимите от a переменную b и результат присвойте переменной c. Затем создайте переменную d, присвойте ей значение 7.
// Сложите переменные c и d, а результат запишите в переменную result. Выведите на экран значение переменной result.

let a = 17
let b = 10
let c = a - b
let d = 7
let result = c + d

console.log(result)

// 2 Строки:
//• Создайте переменную text и присвойте ей значение 'Привет, Мир!'. Выведите значение этой переменной на экран.

let text = "Привет, Мир!"

console.log(text)

// • Создайте переменные text1='Привет, ' и text2='Мир!'. С помощью этих переменных и операции сложения строк выведите на экран фразу 'Привет, Мир!'.

let text1 = "Привет, "
let text2 = "Мир!"

console.log(`${text1} ${text2}`)

// 3 Напишите скрипт, который считает количество секунд в часе, сутках, неделе, месяце из 30 дней.

let secondsInMin = 60
let minsInHour = 60
let hoursInDay = 24

let secondsInHour = secondsInMin * minsInHour
let secondsInDay = secondsInHour * hoursInDay
let secondsInWeek = secondsInDay * 7
let secondsInMonth = secondsInDay * 30

console.log(secondsInHour)
console.log(secondsInDay )
console.log(secondsInWeek)
console.log(secondsInMonth)

// 4 Переделайте приведенный код так, чтобы в нем использовались операции +=, -=, *=, /=, ++, --.
// Количество строк кода при этом не должно измениться! Код для переделки:

// let num = 1;
// num = num + 12;
// num = num - 14;
// num = num * 5;
// num = num / 7;
// num = num + 1;
// num = num - 1;
// console.log(num)

let num = 1;
num = num += 12;
num = num -= 14;
num = num *= 5;
num = num /= 7;
num = num ++;
// num = num --1;

console.log(num)

// 5 Создайте три переменные - час, минута, секунда. С их помощью выведите текущее время в формате 'час:минута:секунда'.

let hour = 2
let minutes = 32
let seconds = 21

console.log(`час: ${hour} минута: ${minutes} секунда ${seconds}`)

// 6 Переделайте этот код так, чтобы в нем использовалась операция +=. Количество строк кода при этом не должно измениться!
//     let text = 'Я';
// text = text +' хочу' ;
// text = text +' знать' ;
// text = text +' JS!' ;
// console.log(text)

let text = 'Я';
text = text += ' хочу';
text = text += ' знать';
text = text += ' JS!';
console.log(text)

// 7 Задана переменная foo = 'bar'; Создать переменную bar, в которой будет храниться число 10. Вывести число 10, используя только переменную foo

let foo = 'bar'
let bar = 10

console.log(eval(foo))


// 8 Еще немного арифметики посложнее  :)
//
//  • Даны два числа. Найти их сумму и произведение.

let a = 10
let b = 2

let sum = a + b
let mult = a * b

console.log(`${sum} , ${mult}`)

// • Даны два числа. Найдите сумму их квадратов.

let a = 10
let b = 2

let a2 = a *= a
let b2 = b *= b

let res = a2 + b2

console.log(res)

// • Даны три числа. Найдите их среднее арифметическое.

let a = 12
let b = 3
let c = 45

let result = (a + b + c) / 3

console.log(result)

// • Даны три числа x,y и z. Найдите (x+1)−2(z−2x+y)

let x = 49
let y = 9
let z = 14

let res = ++x - 2  * (z - 2*x + y)

console.log(res)

// • Дано натуральное числа. Найдите остатки от деления этих чисел на 3 и на 5

let a = 16
let b = 67

let res1 = a % 3
let res2 = a % 5
let res3 = b % 3
let res4 = b % 5

console.log(`${res1},${res2}, ${res3}, ${res4}`)

// . Дано число. Увеличьте его на 30%, на 120%.

let number = 14

let onePer = number / 100
let thirtyPerc = onePer * 30
let res1 = number + thirtyPerc
let res2 = (onePer * 120) + number

console.log (`${res1}, ${res2}`)


// • Дано два числа. Найдите сумму 40% от первого числа и 84% от второго числа.

let num1 = 30
let num2 = 50

let sum = num1 * 0.4 + num2 * 0.84

console.log(sum)

// Дано трехзначное числа. Найдите сумму его цифр.

let num = 123

let strNum= "" + num

let a = parseInt(strNum[0])
let b = parseInt(strNum[1])
let c = parseInt(strNum[2])

let res = a + b + c

console.log(res)

//• Дано трехзначное числа. Поменяйте среднюю цифру на ноль. Найдите число, полученное выписыванием
// в обратном порядке цифр данного трехзначного натурального числа.
let num1 = "123";
let num2= 0;

console.log(num1[0] + num2 + num1[2])

console.log( num1[2] + num1[1] + num1[0])

// 9.
a = 1;      b = 3;
a++ + b;
a + ++b;
++a + b++

console.log(`${a} , ${b}`)


// 10. 3 > 2 ? 'сдал экзамен' : 'не сдал экзамен';
// по этому примеру реализуй три задачи

// - тебе есть 18 лет

let age = 18;
let res = age >= 18 ? "go on" : "no access"
console.log(res)

// - есть ли у тебя загран паспорт

let passport = confirm("do you have international passport?")
let answ = passport ? "you have passp" : "you have no passp"
console.log(answ)


// - есть ли тебе 16

let age = 17;
let res = age == 16 ? "you are already 16" : "you are not 16";
console.log(res);

// 11. задача деление по модулю
// определить какое число четное или не четное. Реализовать через тернарную операцию.
let num = 3
let res = num % 2 == 0 ? " ne 4etnoe" : "4etnoe"

console.log(res)
